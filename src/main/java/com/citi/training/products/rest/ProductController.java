package com.citi.training.products.rest;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.products.service.ProductService;
import com.citi.training.products.model.Product;

@RestController
@RequestMapping(ProductController.BASE_PATH)
public class ProductController {
	
    public final static String BASE_PATH = "/product";
	@Autowired
	ProductService productService;
	
	@RequestMapping(value="/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Product>findAll() {
		return productService.findAll();
	}
	
	@RequestMapping(value="/", method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	//public void create(@RequestBody Product product) {
	public HttpEntity<Product> create(@RequestBody Product product) {
		productService.create(product);
		return new ResponseEntity<Product>(product, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Product findById(@PathVariable int id) {
		return productService.findById(id);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@ResponseBody public void deleteById(@PathVariable int id) {
		productService.deleteById(id);
	}
}
