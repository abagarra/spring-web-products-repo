package com.citi.training.products.service;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.products.dao.ProductDao;
import com.citi.training.products.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {

	@Autowired
	private ProductService productService;
	
	@MockBean
	private ProductDao mockProductDao;
	
	@Test
	public void test_createRuns() {
		
		Product otherProduct = new Product(2, "BreakTest", 1000);
		Product testProduct = new Product(2, "Ham", 9.99);
		
		int newId = 1;
		when(mockProductDao.create(any(Product.class))).thenReturn(newId);
		int createdId = productService.create(testProduct);
		
		verify(mockProductDao).create(testProduct);
		assertEquals(newId,createdId);
		
	}
	
	@Test
	public void test_deleteRuns() {
		int existingId = 1;
		//when(mockProductDao.deleteById(existingId)).thenReturn(existingId);
		//productService.deleteById(existingId);
		
		productService.deleteById(existingId);
		verify(mockProductDao).deleteById(existingId);
	}
	
	@Test
	public void test_findByIdRuns() {
		int testId = 66;
		
		Product testProduct = new Product(66, "Beans", 100.99);
		
		when(mockProductDao.findById(testId)).thenReturn(testProduct);
		
		Product returnedProduct = productService.findById(testId);
		
		verify(mockProductDao).findById(testId);
		assertEquals(testProduct,returnedProduct);
	}
	
	@Test
	public void test_findAll() { 
		List<Product> testList = new ArrayList<Product>();
		testList.add(new Product(33, "LastTest", 999));
		
		when(mockProductDao.findAll()).thenReturn(testList);
		
		List<Product> returnedList = productService.findAll();
		
		verify(mockProductDao).findAll();
		assertEquals(testList, returnedList);
	
	
	}
}
