package com.citi.training.products.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;


import com.citi.training.products.dao.ManufacturerDao;
import com.citi.training.products.model.Manufacturer;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ManufacturerServiceTests {

	@Autowired
	private ManufacturerService manufacturerService;
	
	@MockBean
	private ManufacturerDao mockManufacturerDao;
	
	@Test
	public void test_createRuns() {
		
		Manufacturer otherManufacturer = new Manufacturer(2, "BreakTest", "123 MockTest Dr");
		Manufacturer testManufacturer = new Manufacturer(5, "Ham", "111 FakeAddress Ct");
		
		int newId = 1;
		when(mockManufacturerDao.create(any(Manufacturer.class))).thenReturn(newId);
		int createdId = manufacturerService.create(testManufacturer);
		
		verify(mockManufacturerDao).create(testManufacturer);
		assertEquals(newId,createdId);
		
	}
	
	@Test
	public void test_deleteRuns() {
		int existingId = 1;
		//when(mockManufacturerDao.deleteById(existingId)).thenReturn(existingId);
		//manufacturerService.deleteById(existingId);
		
		manufacturerService.deleteById(existingId);
		verify(mockManufacturerDao).deleteById(existingId);
	}
	
	@Test
	public void test_findByIdRuns() {
		int testId = 66;
		
		Manufacturer testManufacturer = new Manufacturer(66, "SecondFakeAddress", "222 SecondFakeAddress Ct");
		
		when(mockManufacturerDao.findById(testId)).thenReturn(testManufacturer);
		
		Manufacturer returnedManufacturer = manufacturerService.findById(testId);
		
		verify(mockManufacturerDao).findById(testId);
		assertEquals(testManufacturer,returnedManufacturer);
	}
	
	@Test
	public void test_findAll() { 
		List<Manufacturer> testList = new ArrayList<Manufacturer>();
		testList.add(new Manufacturer(9, "LastTest", "333 LastFakeAddress Ct"));
		
		when(mockManufacturerDao.findAll()).thenReturn(testList);
		
		List<Manufacturer> returnedList = manufacturerService.findAll();
		
		verify(mockManufacturerDao).findAll();
		assertEquals(testList, returnedList);
	
	
	}
}
